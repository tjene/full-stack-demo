describe('SignIn', () => {
  beforeEach(() => {
    cy.visit('/sign-in');
  });

  it('should load and render contents', () => {
    cy.get('#card').within($card => {
      cy.contains('Sign In to your Account');
      cy.get('form').within($form => {
        cy.get('input[name="email"]');
        cy.get('input[name="password"]');
        cy.get('button');
      });
    });
  });

  describe('Email Field', () => {
    it('should require properly formed email', () => {
      cy.get('#email-container').within($container => {
        cy.get('input[name="email"]')
          .type('e')
          .type('{backspace}');
        cy.contains('Email is required');

        cy.get('input[name="email"]').type('badEmail');
        cy.contains('Email is invalid');
      });
    });
  });

  describe('Password Field', () => {
    it('should require properly formed password', () => {
      cy.get('#password-container').within($container => {});
      cy.get('input[name="password"]');
    });
  });

  describe('Sign Up Button', () => {
    it('should be disabled unless properly a formed email & password is entered', () => {
      cy.get('button').should('be.disabled');
      cy.get('input[name="email"]').type('testEmail@test.com');
      cy.get('input[name="password"]').type('password');
      cy.get('button').should('be.enabled');
    });
  });
});
