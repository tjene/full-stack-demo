import { colors } from '@material-ui/core';

const white = '#FFFFFF';
const black = '#000000';

export default {
  black,
  white,
  primary: {
    constrastText: white,
    dark: colors.indigo[900],
    main: '#356859',
    light: '#B9E4C9',
  },
  secondary: {
    constrastText: white,
    dark: colors.blue[900],
    main: '#FD5523',
    light: colors.blue['A400'],
  },
  success: {
    constrastText: white,
    dark: colors.green[900],
    main: colors.green[900],
    light: colors.green[900],
  },
  text: {
    primary: colors.blueGrey[900],
  },
  background: {
    default: '#FFFBE6',
  },
};
