import palette from './palette';

export default {
  h1: {
    color: palette.text.primary,
    fontWeight: 5000,
    fontSize: '35px',
    lineHeight: '100px',
  },
};
