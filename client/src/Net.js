const getData = (url) => {
  const response = fetch(url);
  return response;
};

const putData = (data, url) => {
  const response = fetch(url, {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(data),
  });
  return response;
};

const Net = {
  getData,
  putData,
};

export default Net;
