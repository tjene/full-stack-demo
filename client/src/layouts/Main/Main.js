import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/styles';

import { TopBar } from './components';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100%',
  },
  content: {
    paddingTop: '65px',
  },
}));

const Main = (props) => {
  const classes = useStyles();
  const { children } = props;

  return (
    <div className={classes.root}>
      <div className={classes.content}>
        <TopBar />
        <main>{children}</main>
      </div>
    </div>
  );
};

export default Main;
