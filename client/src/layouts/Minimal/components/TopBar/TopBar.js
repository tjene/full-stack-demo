import React from 'react';
import { withStyles } from '@material-ui/styles';
import { AppBar, Toolbar } from '@material-ui/core';
import MenuBookIcon from '@material-ui/icons/MenuBook';

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: 0,
  },
});

const TopBar = (props) => {
  const { classes } = props;

  return (
    <AppBar>
      <Toolbar>
        <MenuBookIcon fontSize="large" />
      </Toolbar>
    </AppBar>
  );
};

export default withStyles(styles)(TopBar);
