import React, { useState, useEffect } from 'react';
import { Link as RouterLink, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import { makeStyles } from '@material-ui/styles';
import {
  Button,
  Card,
  Grid,
  TextField,
  Typography,
} from '@material-ui/core';

import Net from '../../Net';

const schema = {
  email: {
    presence: { allowEmpty: false, message: 'is required' },
    email: { message: 'is invalid' },
    length: {
      maximum: 64,
    },
  },
  password: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      minimum: 8,
      maximum: 128,
    },
  },
};

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.default,
    position: 'fixed',
    height: '100%',
    width: '100%',
    left: 0,
    top: 0,
  },
  button: {
    backgroundColor: theme.palette.secondary.main,
  },
  card: {
    backgroundColor: theme.palette.primary.light,
  },
  form: {
    textAlign: 'center',
    width: '100%',
    padding: '1em',
  },
  grid: {
    height: '100%',
    alignItems: 'center',
  },
}));

const SignUp = (props) => {
  const { history } = props;

  const classes = useStyles();

  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {},
  });

  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState((formState) => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {},
    }));
  }, [formState.values]);

  const loadData = async () => {
    try {
      const url = 'http://localhost:5000/api/users/emails';
      const response = await Net.getData(url);
      const data = await response.json();
      return data;
    } catch (e) {
      console.log(e);
    }
  };

  const handleChange = (e) => {
    e.persist();

    setFormState((formState) => ({
      ...formState,
      values: {
        ...formState.values,
        [e.target.name]: e.target.value,
      },
      touched: {
        ...formState.touched,
        [e.target.name]: true,
      },
    }));
  };

  const checkEmailTaken = (emails, email) => {
    let emailArr = emails.map((email) => email.email);
    return emailArr.includes(email);
  };

  const createAccount = async () => {
    try {
      const url = 'http://localhost:5000/api/users/createUser';
      const response = await Net.putData(formState.values, url);
      const data = await response.json();
      return data;
    } catch (e) {
      console.log(e);
    }
  };

  const handleSignUp = (e) => {
    e.preventDefault();
    loadData().then((val) => {
      let emailTaken = checkEmailTaken(val, formState.values.email);
      if (emailTaken) {
        setFormState((formState) => ({
          ...formState,
          errors: {
            ...formState.errors,
            email: ['Email Address Taken'],
          },
        }));
      } else {
        createAccount().then((res) => {
          history.push('/home');
        });
      }
    });
  };

  const hasError = (field) =>
    formState.touched[field] && formState.errors[field]
      ? true
      : false;

  return (
    <div className={classes.root}>
      <Grid className={classes.grid} container justify="center">
        <Grid item md="auto" xs={11}>
          <Card className={classes.card} id="card" variant="outlined">
            <Grid container justify="center">
              <Grid item xs={12}>
                <Typography align="center">
                  Create your own Account
                </Typography>
              </Grid>
              <form className={classes.form} onSubmit={handleSignUp}>
                <Grid item xs={12} id="email-container">
                  <TextField
                    fullWidth
                    error={hasError('email')}
                    helperText={
                      hasError('email')
                        ? formState.errors.email[0]
                        : null
                    }
                    name="email"
                    onChange={handleChange}
                    placeholder="Email Address"
                    type="text"
                    value={formState.values.email || ''}
                    variant="outlined"
                  />
                </Grid>
                <br />
                <Grid item xs={12} id="password-container">
                  <TextField
                    fullWidth
                    error={hasError('password')}
                    name="password"
                    onChange={handleChange}
                    placeholder="Password"
                    type="password"
                    value={formState.values.password || ''}
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={12}>
                  <Button
                    className={classes.button}
                    disabled={!formState.isValid}
                    type="submit"
                    variant="contained"
                  >
                    Sign up
                  </Button>
                </Grid>
              </form>
            </Grid>
          </Card>
        </Grid>
      </Grid>
    </div>
  );
};

SignUp.propTypes = {
  history: PropTypes.object,
};

export default withRouter(SignUp);
