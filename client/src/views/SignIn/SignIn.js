import React, { useState, useEffect } from 'react';
import { Link as RouterLink, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';
import validate from 'validate.js';
import { makeStyles } from '@material-ui/styles';
import {
  Button,
  Card,
  FormControl,
  Grid,
  Link,
  TextField,
  Typography,
} from '@material-ui/core';

import Net from '../../Net';

const schema = {
  email: {
    presence: { allowEmpty: false, message: 'is required' },
    email: { message: 'is invalid' },
    length: {
      maximum: 64,
    },
  },
  password: {
    presence: { allowEmpty: false, message: 'is required' },
    length: {
      maximum: 128,
    },
  },
};

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.default,
    position: 'fixed',
    height: '100%',
    width: '100%',
    left: 0,
    top: 0,
  },
  button: {
    backgroundColor: theme.palette.secondary.main,
  },
  card: {
    backgroundColor: theme.palette.primary.light,
  },
  form: {
    textAlign: 'center',
    width: '100%',
    padding: '1em',
  },
  grid: {
    height: '100%',
    alignItems: 'center',
  },
}));

const SignIn = (props) => {
  const { history } = props;

  const classes = useStyles();

  const [formState, setFormState] = useState({
    isValid: false,
    values: {},
    touched: {},
    errors: {},
  });

  useEffect(() => {
    const errors = validate(formState.values, schema);

    setFormState((formState) => ({
      ...formState,
      isValid: errors ? false : true,
      errors: errors || {},
    }));
  }, [formState.values]);

  const handleChange = (e) => {
    e.persist();

    setFormState((formState) => ({
      ...formState,
      values: {
        ...formState.values,
        [e.target.name]: e.target.value,
      },
      touched: {
        ...formState.touched,
        [e.target.name]: true,
      },
    }));
  };

  const signIn = async () => {
    try {
      const url = 'http://localhost:5000/api/users/signIn';
      const response = await Net.putData(formState.values, url);
      const jsonBody = await response.json();
      return jsonBody;
    } catch (e) {
      console.log(e);
    }
  };

  const handleSignIn = (e) => {
    e.preventDefault();
    signIn().then((val) => {
      if (val.status === 'Success') history.push('/home');
      else if (val.status === 'Fail') {
        setFormState((formState) => ({
          ...formState,
          errors: {
            ...formState.errors,
            password: ['Wrong Password'],
          },
        }));
      }
    });
  };

  const hasError = (field) =>
    formState.touched[field] && formState.errors[field]
      ? true
      : false;

  return (
    <div className={classes.root}>
      <Grid className={classes.grid} container justify="center">
        <Grid item md="auto" xs={11}>
          <Card className={classes.card} id="card" variant="outlined">
            <Grid container justify="center">
              <Grid item xs={12}>
                <Typography align="center">
                  Sign In to your Account
                </Typography>
              </Grid>
              <form className={classes.form} onSubmit={handleSignIn}>
                <Grid item xs={12} id="email-container">
                  <TextField
                    fullWidth
                    error={hasError('email')}
                    helperText={
                      hasError('email')
                        ? formState.errors.email[0]
                        : null
                    }
                    id="email"
                    name="email"
                    onChange={handleChange}
                    placeholder="Email Address"
                    type="text"
                    value={formState.values.email || ''}
                    variant="outlined"
                  />
                </Grid>
                <br />
                <Grid item xs={12} id="password-container">
                  <TextField
                    fullWidth
                    error={hasError('password')}
                    helperText={
                      hasError('password')
                        ? formState.errors.password[0]
                        : null
                    }
                    id="password"
                    name="password"
                    onChange={handleChange}
                    placeholder="Password"
                    type="password"
                    value={formState.values.password || ''}
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={12}>
                  <Button
                    className={classes.button}
                    disabled={!formState.isValid}
                    id="submit"
                    type="submit"
                    variant="contained"
                  >
                    Sign in
                  </Button>
                </Grid>
              </form>
              <Typography>
                <Link
                  component={RouterLink}
                  to="/sign-up"
                  variant="h6"
                >
                  Create an Account
                </Link>
              </Typography>
            </Grid>
          </Card>
        </Grid>
      </Grid>
    </div>
  );
};

SignIn.propTypes = {
  history: PropTypes.object,
};

export default withRouter(SignIn);
