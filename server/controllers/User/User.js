import { User as UserModel } from '../../models';

const bcrypt = require('bcrypt');

const hash = async (plainPass) => {
  const hashString = await bcrypt.hash(plainPass, 10);
  return hashString;
};

const compare = async (dbPass, pass) => {
  const match = await bcrypt.compare(pass, dbPass);

  if (match) return true;

  return false;
};

const createUser = (email, password, done) => {
  hash(password).then((hash) => {
    UserModel.createUser(email, hash, done);
  });
};

const signIn = (email, password, done) => {
  UserModel.searchUser(email, (err, record) => {
    if (err) {
      console.log(err);
    } else {
      if (record.length > 0)
        compare(record[0].password, password).then((match) => {
          if (match) {
            done(null, 'Success');
          } else {
            done(null, 'Fail');
          }
        });
      else done(null, 'Invalid Email');
    }
  });
};

const getAll = (done) => {
  UserModel.getAll(done);
};

const getAllEmails = (done) => {
  UserModel.getAllEmails(done);
};

const User = {
  createUser,
  getAll,
  getAllEmails,
  hash,
  signIn,
};

export default User;
