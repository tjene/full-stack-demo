require('dotenv').config();
import mysql from 'mysql';
import async from 'async';

const PRODUCTION_DB = 'FullStackDemoPROD';
const TEST_DB = 'FullStackDemo';

const state = {
  pool: null,
  mode: null,
};

const connect = (mode, done) => {
  state.pool = mysql.createPool({
    host: process.env.HOST,
    user: process.env.DBUSER,
    password: process.env.PASSWORD,
    database:
      process.env.NODE_ENV === 'production' ? PRODUCTION_DB : TEST_DB,
  });

  state.mode = mode;
  done();
};

const get = () => {
  return state.pool;
};

const fixtures = (data, done) => {
  let pool = state.pool;
  if (!pool) return done(new Error('Missing database connection.'));

  let names = Object.keys(data.tables);
  async.each(
    names,
    (name, cb) => {
      async.each(
        data.tables[name],
        (row, cb) => {
          let keys = Object.keys(row),
            values = keys.map(function (key) {
              return "'" + row[key] + "'";
            });

          pool.query(
            'INSERT INTO ' +
              name +
              ' (' +
              keys.join(',') +
              ') VALUES (' +
              values.join(',') +
              ')',
            cb,
          );
        },
        cb,
      );
    },
    done,
  );
};

const drop = (tables, done) => {
  let pool = state.pool;
  if (!pool) return done(new Error('Missing database connection.'));

  async.each(
    tables,
    (name, cb) => {
      pool.query('DELETE * FROM ' + name, cb);
    },
    done,
  );
};

const DB = {
  connect,
  drop,
  get,
  fixtures,
};

export default DB;
