import { Router } from 'express';

import { User as UserController } from '../../controllers';

const router = Router();

router.get('/', (req, res, next) => {
  UserController.getAll((err, record) => {
    if (err) console.log(err);
    console.log(res.body);
    res.send(record);
  });
});

router.put('/createUser', (req, res, next) => {
  UserController.createUser(
    req.body.email,
    req.body.password,
    (err) => {
      if (err) console.log(err);
      res.json({ status: 'Success' });
    },
  );
});

router.put('/signIn', (req, res, next) => {
  UserController.signIn(
    req.body.email,
    req.body.password,
    (err, record) => {
      if (err) console.log(err);
      res.json({ status: record });
    },
  );
});

router.get('/emails', (req, res, next) => {
  UserController.getAllEmails((err, record) => {
    if (err) console.log(err);
    res.send(record);
  });
});

export default router;
