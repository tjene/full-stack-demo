import DB from '../../db';

const getAll = (done) => {
  DB.get().query('SELECT * FROM user', (err, rows) => {
    if (err) {
      done();
    }
    console.log(rows);
    done(null, rows);
  });
};

const searchUser = (email, done) => {
  let test = 1;
  let sql = `SELECT * FROM user WHERE email = \'${email}\'`;
  DB.get().query(sql, (err, rows) => {
    if (err) done();
    done(null, rows);
  });
};

const getAllEmails = (done) => {
  DB.get().query('SELECT email from user', (err, rows) => {
    if (err) {
      return done;
    }
    done(null, rows);
  });
};

const createUser = (email, password, done) => {
  let values = [email, password];
  let sql = 'INSERT INTO user (email, password) VALUES (?, ?)';
  DB.get().query(sql, values, (err, result) => {
    if (err) return done(err);
    done(null, result.insertId);
  });
};

const User = {
  createUser,
  getAll,
  getAllEmails,
  searchUser,
};

export default User;
