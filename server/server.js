require('dotenv').config();
const path = require('path');
import express from 'express';
import cors from 'cors';
import { User as UserRoute } from './routes';
import DB from './db';
const bodyParser = require('body-parser');
const app = express();

app.use(cors());
app.use(bodyParser.json());

app.get('/api', (req, res, next) => {
  res.json({ data: 'Back End' });
});

app.use('/api/users', UserRoute);

DB.connect(process.env.NODE_ENV, (err) => {
  if (err) {
    console.log('Unable to connect to MySQL.');
    process.exit(1);
  } else {
    app.listen(process.env.PORT, () => {
      console.log(
        `Backend API listening on port ${process.env.PORT}`,
      );
    });
  }
});
