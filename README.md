# Full Stack App Demo

### Tech

- [Babel] - JavaScript compiler
- [bcrypt] - password hashing
- [Cypress] - frontend testing
- [ESLint] - JavaScript Linter
- [Express] - backend web app framework
- [Material-UI] - React UI framework
- [Node.js] - evented I/O for the backend
- [prettier] - code formatter
- [Webpack] - module bundler

### Install Pre-reqs

If you don't have homebrew installed yet

```sh
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Install Prettier/Configure (VS Code)

```sh
$ npm install -g prettier
```

- [Install VS Code Extension]
- Configure Prettier to format on Save (Optional)
  -- Add the following configuration to VS Code user settings/preferences

```sh
// Set the default
"editor.formatOnSave": false,
// Enable per-language
"[javascript]": {
    "editor.formatOnSave": true
}
```

I think you can do it through a GUI too, idk.

### MySQL Setup

Install & configure MySQL Server

```sh
$ brew install mysql
$ brew services start mysql
$ mysqladmin -u root password 'root'
$ mysql -u root -p
> ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'root';
> flush privileges;
```

Set up demo DB and table

```sh
> create database FullStackDemo;
> use FullStackDemo;
> CREATE TABLE user (
id int NOT NULL AUTO_INCREMENT,
email varchar(255) NOT NULL,
password varchar(255) NOT NULL,
PRIMARY KEY (id)
);
> quit
```

### Install Demo

```sh
$ git clone https://cpbingham@bitbucket.org/tjene/full-stack-demo.git
$ cd full-stack-demo
$ npm install
$ cd client && npm install
$ cd ../server && npm install
$ cd ..
```

### Run in Dev Mode

```sh
$ npm start
```

Visit [http://localhost:8080/]

### Run in Production

```sh
$ npm run build
$ npm run deploy
```

Visit [http://localhost:3000/]

### Running Tests

```sh
$ npm run test:cypress
```

### Analyze Bundle Size

Powered by [webpack-bundle-analyzer]
More on [webpack plugins]

```sh
$ npm run build:analyze
$ npm run deploy
```

Visit the following links:

- [webpack-report]

* [webpack-stats]

### Next Steps

- Backend Testing
- Additional Network Validation/testing
- Using Webpack for code splitting
- General Clean up/commenting

[babel]: https://babeljs.io/
[bcrypt]: https://www.npmjs.com/package/bcrypt
[cypress]: https://www.cypress.io/
[eslint]: https://eslint.org/
[express]: https://expressjs.com/
[node.js]: http://nodejs.org
[material-ui]: https://material-ui.com/
[prettier]: https://prettier.io/
[webpack]: https://webpack.js.org/
[webpack-bundle-analyzer]: https://www.npmjs.com/package/webpack-bundle-analyzer
[webpack-report]: http://localhost:3000/report.html
[webpack-stats]: http://localhost:3000/stats.html
[webpack plugins]: https://webpack.js.org/plugins/
[http://localhost:8080/]: http://localhost:8080/
[http://localhost:3000/]: http://localhost:3000/
[install vs code extension]: https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode
